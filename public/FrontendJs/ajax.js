$(document).ready(function() {

    function ajaxPost() {

        // PREPARE FORM DATA
        var formData = {
            firstname: $("#firstname").val(),
            lastname: $("#lastname").val()
        }

        // DO POST
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: window.location + "api/customers/save",
            data: JSON.stringify(formData),
            dataType: 'json',
            success: function(customer) {
                $("#postResultDiv").html("<p>" +
                    "Post Successfully! <br>" +
                    "--->" + JSON.stringify(customer) + "</p>");
            },
            error: function(e) {
                alert("Error!")
                console.log("ERROR: ", e);
            }
        });

    }
});