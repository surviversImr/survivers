
const User = require('../model/User.js');
const passport = require('passport');
const passwordHash = require('password-hash');

exports.login = function (req, res, next) {

    res.render('login', { title: "login" })
}
exports.submitlogin = function (req, res, next) {

    var listGame = Array({ id: "1", name: "puissance 4" }, { id: "2", name: "puissance 5" }, { id: "1", name: "puissance 4" }, { id: "2", name: "puissance 5" });

    res.render('list', { listGame })


}
exports.register = function (req, res, next) {
    res.render('register', { title: 'register' });
}
exports.registerPost = async function (req, res, next) {

    try {
        var hashedPassword = passwordHash.generate(req.body.pwd);
        var user = new User(req.body.pseudo, hashedPassword, req.body.email);
        user.create();
        res.redirect("/login");
    } catch (e) {
        res.redirect("/register");
    }



}

/**
 * https://www.youtube.com/watch?v=-RCnNyD0L-s
 */

