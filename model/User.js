const lowdb = require('lowdb');
const { v4: uuid } = require('uuid');
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('Data/mydata.json')
const db = lowdb(adapter)
const bcrypt = require('bcrypt')

class User {
    //constructeur prends pseudo et mot de pass et email
    constructor(pseudo, pswHash, email) {
        this.id = uuid();
        this.pseudo = pseudo;
        this.pswHash = bcrypt.hashSync(pswHash, bcrypt.genSaltSync(10));
        this.email = email;
        this.role = 'user';
        this.Status = 'NonBlocked';
        this.Games = Array();
    };

    static addScore(id, game, value) {
        console.log(id + " " + game + " " + value)
        const bestGame = db.get('Users')
            .find({ id: id }) // Lodash shorthand syntax
            .get('Games')
            .find({ id: game.id })
            .value();
        console.log('bestGame : ' + bestGame);

        if (bestGame != null || bestGame > value) {
            console.log('update');
            return db.get('Users')
                .find({ id: id }) // Lodash shorthand syntax
                .get('Games')
                .find({ id: game.id })
                .set({ score: value })
                .write();
        }
    };


    //ajouter un jeu a un joueur  par l'id de joueur et les donnees de ce jeu
    static addGame(idIn, game) {
        var ngame = {
            "id": game.id,
            "name": game.name,
            "icon": game.icon,
            "url": game.url,
            "score": null
        };

        //retourner les jeux de ce joueur
        return db.get('Users')
            .find({ id: idIn }) // Lodash shorthand syntax
            .get('Games')
            .push(ngame)
            .write()
    };
    //return les données de ce joueur
    get = function() {
            return { id: this.id, pseudo: this.pseudo, pswHash: this.pswHash, email: this.email, role: this.role, Status: this.Status, Games: this.Games } //
        }
        //pour initilaser le fichier json lowdb
    init = function() {
        // Set some defaults
        db.defaults({
                Games: [],
                Users: []
            })
            .write()
    };

    //inserer des donnees dans users
    create = function(callback) {
        var res = 0,
            err = 0;
        try {
            db.get('Users')
                .push(this.get())
                .write()
            callback(err, this.get())
        } catch (err) {
            callback(err, res)
        }
    };

    //recuperer joueur via son id 
    static getByID = function(idIn, callback) {
        var res = 0,
            err = 0;
        try {
            res = db.get('Users')
                .find({ id: idIn })
                .value();
            callback(err, res)
        } catch (err) {
            callback(err, res)
        }
    };
    //recipere via son pseudo 
    static getByPseudo = function(pseudoIn, callback) {
        var res = 0,
            err = 0;
        try {
            res = db.get('Users')
                .find({ pseudo: pseudoIn })
                .value();
            callback(err, res)
        } catch (err) {
            callback(err, res)
        }



    };

    //bloquer un joueur via son id 
    static block = function(idIn) {
        return db.get('Users')
            .find({ id: idIn })
            .assign({ Status: 'Blocked' })
            .write()
    }

    //debloquer un joueur via son id
    static deblock = function(idIn) {
            return db.get('Users')
                .find({ id: idIn })
                .assign({ Status: 'NonBlocked' })
                .write()
        }
        //un method static pour recuperer les donnees  
    static getAll = function() {
        return db.get('Users').value();
    };

    //mis a jour un joueur
    static update = function(idd) {
        db.get('Users')
            .find({ id: idd })
            .assign(Status, 'Blocked')
            .write()
    }

    //
    update = function() {
            db.get('Users')
                .find({ id: this.id })
                .value()
        }
        //supprimer un joueur 
    static remove = function(idd) {
        db.get('Users')
            .remove({ id: idd })
            .write()
    };
    //pour comaprer un mdp
    static comparingPasswords = function(password, hash) {
        return bcrypt.compareSync(password, hash);
    }

}

module.exports = User;