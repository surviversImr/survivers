const lowdb = require('lowdb');
const { v4: uuid } = require('uuid');
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('Data/mydata.json')
const db = lowdb(adapter)

class Game {
    //constructeur prens en paramter name et url de jeu et image et des infos
    constructor(name, url, info, urlimg) {
        this.id = uuid();
        this.name = name;
        this.urlimg = urlimg;
        this.info = info;
        this.url = url;
    }

    get = function() {
        return this;
    };

    init = function() {
        // Set some defaults
        db.defaults({
                Games: [],
                Users: []
            })
            .write()
    };
    //pour inserer les donnees dans lowdb 'json fichier'
    create = function() {
        // Add a post
        db.get('Games')
            .push(this.get())
            .write()
    };

    //recuperer le jeu via son id

    static getByID = function(idIn, callback) {
        var res = 0,
            err = 0;
        try {
            res = db.get('Games')
                .find({ id: idIn })
                .value()
            callback(err, res);
        } catch (e) {
            callback(err, res);
        }

    };

    //recuperer le jeu via son name
    static getByName = function(nameIn) {
        return db.get('Games').find({ name: nameIn }).value();
    };
    //recuperer tous les jeux
    static getAll = function() {
        return db.get('Games').value();
    };
    ///mis ajour d'un jeu
    update = function(idd) {
        db.get('Games')
            .find({ id: idd })
            .value()
    };
    //supprimer un jeu
    remove = function(idd) {
        db.get('Games')
            .remove({ id: idd })
            .write()
    };
}


module.exports = Game;