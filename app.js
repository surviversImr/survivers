var http = require('http');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport')
const flash = require('express-flash')
const session = require('express-session')


const passportsetup = require('./config/passport-setup')
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(flash())

app.use(session({
    secret: 'toto',
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 60000 * 15 }
}))
app.use(passport.initialize())
app.use(passport.session())
    //les routes par fichier 

// routes user login signup ..
const users = require('./routes/user-routes');
app.use('/users', users);

// routes list de jeux et ajouetr un jeu
const games = require('./routes/game-routes');
app.use('/games', games);

// routes lsit de joueurs et bloquer et deblqouer un joueur par admin
const admins = require('./routes/admin-routes');
app.use('/admin', admins);
//les dernier route contein  que la route vers index
const indexRouter = require('./routes/route');
app.use('/', indexRouter);


const server = app.listen(8081, function() {
    const host = server.address().address
    const port = server.address().port
    console.log("notre serveur fonction sur la port  http://%s:%s", host, port)
})
module.exports = app;