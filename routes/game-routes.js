const express = require('express');
const router = express.Router();
const passport = require('passport');
const Game = require('../model/Game');
const User = require('../model/User');
//verifier si le user est authentifié

function checkAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }
    res.redirect('/users/login')
}

//list de jeux
router.get('/', checkAuthenticated, (req, res) => {
    res.render('game/Games', { Games: Game.getAll() })
})

//ajouter un jour a mes jeux preferes
router.post('/addGame', checkAuthenticated, (req, res) => {
    let id = req.body.id; //recuper le jeux via son id 
    Game.getByID(id, (err, game) => {
        if (err) {
            console.log("error")
        }
        if (game) {
            User.addGame(req.user.id, game); //ajouetr le jeu a la list de jeu preferé
            // res.render('game/Games', { Games: Game.getAll(), isconnect: req.isAuthenticated() })
            console.log(req.isAuthenticated());
            res.redirect('/users/profile');
        }
    });
})

<<<<<<< HEAD
///
=======
router.get('/plusoumoins', checkAuthenticated, (req, res) => {
    const jeu = Game.getByName("Plus ou Moins");
    console.log(jeu);
    res.render('game/PlusOuMoins', { user: req.user, game: jeu });
})

>>>>>>> 011fdffa7ff73cb7d04a2cafa2fd2d5bfacabdbc
router.post('/plusoumoins', checkAuthenticated, (req, res) => {

    User.addScore(req.user.id, { id: req.body.idGame }, req.body.score);
    res.redirect('/games/plusoumoins');

})

router.get('/plusoumoins2', checkAuthenticated, (req, res) => {
    const jeu = Game.getByName("Plus ou Moins 2");
    console.log(jeu);
    res.render('game/PlusOuMoins2', { user: req.user, game: jeu });
})

router.post('/plusoumoins2', checkAuthenticated, (req, res) => {

    User.addScore(req.user.id, { id: req.body.idGame }, req.body.score);
    res.redirect('/games/plusoumoins2');

})


router.get('/plusoumoins3', checkAuthenticated, (req, res) => {
    const jeu = Game.getByName("Plus ou Moins 3");
    console.log(jeu);
    res.render('game/PlusOuMoins3', { user: req.user, game: jeu });
})

router.post('/plusoumoins3', checkAuthenticated, (req, res) => {

    User.addScore(req.user.id, { id: req.body.idGame }, req.body.score);
    res.redirect('/games/plusoumoins3');

})


router.get('/plusoumoins4', checkAuthenticated, (req, res) => {
    const jeu = Game.getByName("Plus ou Moins 4");
    console.log(jeu);
    res.render('game/PlusOuMoins4', { user: req.user, game: jeu });
})

router.post('/plusoumoins4', checkAuthenticated, (req, res) => {

    User.addScore(req.user.id, { id: req.body.idGame }, req.body.score);
    res.redirect('/games/plusoumoins4');

})



module.exports = router;