const express = require('express');
const router = express.Router();
const passport = require('passport');
const User = require('../model/User');

//verifier si le user est authentifié

function checkAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }
    res.redirect('/users/login')
}

function checkNotAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return res.redirect('/users/profile')
    }
    next()
}
//login view
router.get('/login', checkNotAuthenticated, function(req, res, next) {
    res.render('user/login', {
        error: req.flash('error')
    })
});
//login submit
router.post('/login',
        passport.authenticate('local.login', {
            successRedirect: '/users/profile',
            failureRedirect: '/users/login',
            failureFlash: true
        })

        /**/
    )
    //register view
router.get('/register', checkNotAuthenticated, function(req, res, next) {
    res.render('user/register', {
        error: req.flash('error')
    })
});
//register submit
router.post('/register',
    passport.authenticate('local.register', {
        successRedirect: '/users/profile',
        failureRedirect: '/users/register',
        failureFlash: true
    }));

//profile
router.get('/profile', checkAuthenticated, (req, res) => {
    User.getByID(req.user.id, (err, _user_) => { //recuperer le user par son id dans session 
        if (_user_) { //si il y a bien un joueur de cet id
            res.render('user/profile', {
                success: req.flash('success'), //afficher un message de resuuite
                test: req.isAuthenticated(),
                user: _user_
            })
        }
        if (err) {
            //si il y a un erreur mais normalement il  n'y aura pas car ca sera capté dans le login
        }

    })

})


// logout user

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/users/login');
})

router.get('/github',
    passport.authenticate('github'));

router.get('/register/callback',
    passport.authenticate('github', { failureRedirect: '/login' }),
    function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/');
    });
module.exports = router;