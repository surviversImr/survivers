const express = require('express');
const router = express.Router();

/***
 * s
 */

const game = require('../controlers/game');
const user = require('../controlers/user');


/* GET home page. */
router.get('/', checkAuthenticated, function(req, res, next) {
    res.render('index', { title: 'Survivers' });
})

//verifier si le user est authentifié

function checkAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }
    res.redirect('/users/login')
}



module.exports = router;