const express = require('express');
const router = express.Router();
const passport = require('passport');
const Game = require('../model/Game');
const User = require('../model/User');



//verifeier si le user est authentifié
function checkAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }
    res.redirect('/users/login')
}

//vers la page d'admin contenant lsite de joueurs 
router.get('/admin', checkAuthenticated, (req, res) => {
    console.log(User.getAll());
    console.log('test');
    res.render('admin/admin', { Users: User.getAll() })
})

//pour blouer un joueur
router.post('/blockUser', checkAuthenticated, (req, res) => {
    User.block(req.body.id); //blqouer via lid 
    res.render('admin/admin', { Users: User.getAll() }) //recharger la page
})

//debloquer un joueur
router.post('/deblockUser', checkAuthenticated, (req, res) => {
    User.deblock(req.body.id); //debloquer via lid 
    res.render('admin/admin', { Users: User.getAll() }) //recharger la page
})




module.exports = router;