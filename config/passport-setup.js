const passport = require('passport');
const localStrategy = require('passport-local').Strategy
var GitHubStrategy = require('passport-github').Strategy;
const User = require('../model/User')


// saving user object in the session

passport.serializeUser(function(user, done) {

    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.getByID(id, function(err, user) {
        done(err, user);
    });
});

passport.use('local.register', new localStrategy({
    usernameField: 'pseudo',
    passwordField: 'pwd',
    passReqToCallback: true
}, (req, username, password, done) => {

    if (req.body.pwd != req.body.confirmPwd) {

        return done(null, false, req.flash('error', 'password do not match'));
    } else {
        User.getByPseudo(username, (err, user) => {
            if (err) {

                return done(err)
            } else if (user) {
                return done(null, false, req.flash('error', 'pseudo already used'))
            } else {
                let user = new User(username, password, req.body.email);
                user.create((err, user) => {
                    if (!err) {

                        return done(null, user, req.flash('success', 'User Added'))
                    } else {
                        return done(err);
                    }
                });
            }
        })

    }
}));



//login strategy

passport.use('local.login', new localStrategy({
    usernameField: 'pseudo',
    passwordField: 'pwd',
    passReqToCallback: true
}, (req, username, password, done) => {

    User.getByPseudo(username, (err, user) => {

        if (err) {

            return done(null, false, req.flash('error', 'Something wrong happened' + err))
        } else if (!user) {

            return done(null, false, req.flash('error', 'user was not found'))
        } else if (user) {
            if (user.Status == 'Blocked') {
                return done(null, false, req.flash('error', ' Sorry you have been blocked by admin'))
            }
            if (User.comparingPasswords(password, user.pswHash)) {
                console.log("success  welcome back" + user.id);
                return done(null, user, req.flash('success', ' welcome back'))

            } else {
                console.log(" bad boy" + user.id);
                return done(null, false, req.flash('error', ' password is wronggggg'))

            }
        }
    })

}))

//github strategy

passport.use(new GitHubStrategy({
        clientID: '14698e3fc606cbd4fdac',
        clientSecret: '500b0d8ab31bf7619bed37e4e51e7b3c97b1d163',
        callbackURL: "http://localhost:8081/users/register/callback"
    },
    function(accessToken, refreshToken, profile, cb) {
        console.log('rrrrrrrrr')
        cb(null)
            /*  User.findOrCreate({ githubId: profile.id }, function (err, user) {
                return cb(err, user);
              });*/
    }
));